## Build executable
cargo build --release

## Test config file

mkdir tmp

# Missing config file
echo "Testing without config file --- Should panick with \"Missing config file\""
mv config/config.json ./tmp/

./target/release/cdn_dispatcher

## wrong config file

cat ./tmp/config.json > config/config.json
echo "yo" >> config/config.json

echo "Testing with wrongly formated config file --- Should panick with \"trailing characters\""
./target/release/cdn_dispatcher


## put back config
mv ./tmp/config.json ./config/

rm -rf ./tmp

## Testing without ip -- should work and signedUrl the path
echo "Testing without ip -- should work and signedUrl the path"
echo -e "GET /?country=FRA&path=yoyo HTTP/1.0\r\n" | nc -U "/tmp/static0.sock"

## Testing without country -- should work and signedUrl the path
echo -e "GET /?ip=109.29.135.38&path=yoyo HTTP/1.0\r\n" | nc -U "/tmp/static0.sock"

## Testing without ip and country -- should res send error message : "missing ip and country"                     
echo -e "GET /?path=yoyo HTTP/1.0\r\n" | nc -U "/tmp/static0.sock"

## Testing without path -- nothing to sign ! error message : ""missing path"

echo -e "GET /?country=BLR&path=FRA/2020/48ba1787-55b6-4311-ba71-a6cd1ed64e9c.mp3 HTTP/1.0\r\n" | nc -U "/tmp/static0.sock" #| curl


## test with curl


# check valid signedurl ovh/cloudfront/stackpath

## Testing europe -- Should receive a signed url to OVH

echo "Testing europe -- Should receive a signed url to OVH"

## Test valid params -- should output HTTP/2 200
echo "With correctly formatted path -- should 200"
echo -e "GET /?country=BLR&path=FRA/2020/48ba1787-55b6-4311-ba71-a6cd1ed64e9c.mp3 HTTP/1.0\r\n" | nc -U "/tmp/static0.sock" | xargs curl -I 2>/dev/null | grep HTTP

## test wrong path -- should output HTTP/2 403
echo "With wrong formatted path -- should 403"
echo -e "GET /?country=BLR&path=FRA/2020/48ba1787-55b6-4311-ba71-a6cd1ed64e9c.mp3_WRONG HTTP/1.0\r\n" | nc -U "/tmp/static0.sock" | xargs curl -I 2>/dev/null | grep HTTP

## Testing Americas -- Should receive a signed url to Stackpath

echo "Testing Americas -- Should receive a signed url to Stackpath"
## Test valid params -- should output HTTP/2 200
echo "With correctly formatted path -- should 200"
echo -e "GET /?country=USA&path=FRA/2020/48ba1787-55b6-4311-ba71-a6cd1ed64e9c.mp3 HTTP/1.0\r\n" | nc -U "/tmp/static0.sock" | xargs curl -I 2>/dev/null | grep HTTP

## test wrong path -- should output HTTP/2 403
echo "With wrong formatted path -- should 403"
echo -e "GET /?country=USA&path=FRA/2020/48ba1787-55b6-4311-ba71-a6cd1ed64e9c.mp3_WRONG HTTP/1.0\r\n" | nc -U "/tmp/static0.sock" | xargs curl -I 2>/dev/null | grep HTTP

## Testing rest of the world -- Should receive a signed url to CloudFront

echo "Testing rest of the world -- Should receive a signed url to CloudFront"
## Test valid params -- should output HTTP/2 200
echo "With correctly formatted path -- should 200"
echo -e "GET /?country=KOR&path=FRA/2020/48ba1787-55b6-4311-ba71-a6cd1ed64e9c.mp3 HTTP/1.0\r\n" | nc -U "/tmp/static0.sock" | xargs curl -I 2>/dev/null | grep HTTP

## test wrong path -- should output HTTP/2 403
echo "With wrong formatted path -- should 403"
echo -e "GET /?country=KOR&path=FRA/2020/48ba1787-55b6-4311-ba71-a6cd1ed64e9c.mp3_WRONG HTTP/1.0\r\n" | nc -U "/tmp/static0.sock" | xargs curl -I 2>/dev/null | grep HTTP

