use actix_web::{middleware, web, App, HttpServer};

mod dispatcher;
use dispatcher::index;
use ip2country::AsnDB;

mod config;
mod countries;
mod signed_url;
use config::{get_cdn_config, get_config, update, Config};

use std::collections::HashMap;
use std::sync::{Arc, Mutex};

#[derive(Clone)]
pub struct AppState {
    config: Config,
    ip_db: Arc<AsnDB>,
    cdn_config: Arc<
        Mutex<(
            HashMap<String, fn(&str, &Config) -> String>,
            HashMap<String, fn(&str, &Config) -> String>,
        )>,
    >,
}

#[actix_web::main]
#[cfg(unix)]
async fn main() -> std::io::Result<()> {
    ::std::env::set_var("RUST_LOG", "actix_server=info,actix_web=info");
    env_logger::init();
    let config = get_config();
    let cdn_config = get_cdn_config();
    let port = config.app.port.to_string();
    let data = AppState {
        config: config,
        ip_db: std::sync::Arc::new(
            AsnDB::default()
                .load_ipv4(
                    "./ip-location-db/geo-whois-asn-country/geo-whois-asn-country-ipv4-num.csv",
                )
                .expect("Could not load ip database"),
        ),
        cdn_config: std::sync::Arc::new(Mutex::new(cdn_config)),
    };

    HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::new("%D ms"))
            .data(data.clone())
            .service(web::resource("/").route(web::get().to(index)))
            .service(web::resource("/update").route(web::get().to(update)))
    })
    .bind_uds(port)?
    .run()
    .await
}

#[cfg(not(unix))]
fn main() -> std::io::Result<()> {
    println!("not supported");
    Ok(())
}
