use super::config::Config;

// Utility functions to get signed url expiry time as microseconds and seconds

use std::time::{SystemTime, UNIX_EPOCH};
fn get_expiry_time() -> u128 {
    return SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_micros()
        + 900;
}
fn get_expiry_time_secs() -> u64 {
    return SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs()
        + 900;
}

// Stackpath signing

pub fn stackpath(path: &str, config: &Config) -> String {
    let stackpath = config.stackpath.as_ref().unwrap();
    let expiry = get_expiry_time();
    let signed_url = format!(
        "/{}?expires={}&{}={}",
        path, expiry, stackpath.passphrasefield, stackpath.passphrase
    );
    let digest = md5::compute(signed_url);
    let url = format!(
        "{}{}?expires={}&{}={:?}",
        stackpath.url, path, expiry, stackpath.tokenfield, digest
    );
    return url;
}

// CLOUDFRONT SIGNING

use std::path::Path;

fn read_file(path: &std::path::Path) -> Result<Vec<u8>, MyError> {
    use std::io::Read;

    let mut file = std::fs::File::open(path).map_err(|e| MyError::IO(e))?;
    let mut contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut contents)
        .map_err(|e| MyError::IO(e))?;
    Ok(contents)
}

#[derive(Debug)]
pub enum MyError {
    IO(std::io::Error),
}

use openssl::base64::encode_block;
use openssl::hash::MessageDigest;
use openssl::pkey::PKey;
use openssl::sign::Signer;

pub fn get_cloudfront_key_pem(private_key_path: &str) -> Vec<u8> {
    let path = Path::new(private_key_path);
    let private_key_pem = read_file(path).expect("Could not read private key pem");
    return private_key_pem;
}

fn sign_and_encode(message: &str, private_key_pem: &Vec<u8>) -> Result<String, MyError> {
    let key_pair = PKey::private_key_from_pem(&private_key_pem)
        .expect("Could not create key_pair from private key pem");
    let mut signer = Signer::new(MessageDigest::sha1(), &key_pair).unwrap();
    signer.update(message.as_bytes()).unwrap();
    let signature = signer.sign_to_vec().unwrap();
    let encoded = encode_block(&signature);
    let converted: String = encoded
        .chars()
        .map(|x| match x {
            '+' => '-',
            '=' => '_',
            '/' => '~',
            _ => x,
        })
        .collect();
    Ok(converted)
}

pub fn cloudfront(path: &str, config: &Config) -> String {
    let cloudfront = config.cloudfront.as_ref().unwrap();
    let url = format!("{}{}", cloudfront.url, path);
    let expiry = get_expiry_time();
    let canned_policy = format!("{{\"Statement\":[{{\"Resource\":\"{}\",\"Condition\":{{\"DateLessThan\":{{\"AWS:EpochTime\":{:?}}}}}}}]}}", url, expiry);

    let signature = match sign_and_encode(
        &canned_policy,
        &cloudfront.private_key_pem.as_ref().unwrap(),
    ) {
        Result::Ok(val) => val,
        Result::Err(err) => {
            panic!("Unable signed url for cloudfront: {:?}", err)
        }
    };
    let signed_url = format!(
        "{}?Expires={}&Key-Pair-Id={}&Signature={}",
        url, expiry, cloudfront.access_key, signature
    );
    return signed_url;
}

// Bunny CDN

pub fn bunny(path: &str, config: &Config) -> String {
    let bunny = config.bunny.as_ref().unwrap();
    let expiry = get_expiry_time_secs().to_string();
    let hash_base = format!("{}/{}{}", bunny.access_key, path, expiry);
    let digest = md5::compute(&hash_base);
    let encoded = encode_block(&digest.to_vec());
    let converted: String = encoded
        .replace("\n", "")
        .replace("=", "")
        .chars()
        .map(|x| match x {
            '+' => '-',
            '/' => '_',
            _ => x,
        })
        .collect();

    let signed_url = format!(
        "{}{}?token={}&expires={}",
        bunny.url, path, converted, expiry
    );
    return signed_url;
}
