use super::config::Config;
use super::signed_url::cloudfront;
use super::AppState;
use actix_web::{web, HttpResponse};
use serde::Deserialize;
use std::net::IpAddr;
use std::str::FromStr;

#[derive(Deserialize)]
pub struct Query {
    ip: Option<String>,
    country: Option<String>,
    path: Option<String>,
}

fn dispatch(country: &str, query: &Query, data: &AppState) -> String {
    let config = &data.config;
    let cdn_from_iso = &data.cdn_config.lock().unwrap();
    let path = &query.path.as_ref().unwrap();

    let cdn_from_iso3 = &cdn_from_iso.0;
    let cdn_from_iso2 = &cdn_from_iso.1;

    return cdn_from_iso3.get(country).unwrap_or_else(|| {
        cdn_from_iso2
            .get(country)
            .unwrap_or_else(|| &(cloudfront as fn(&str, &Config) -> String))
    })(path, &config);
}

pub async fn index(query: web::Query<Query>, data: web::Data<AppState>) -> HttpResponse {
    let ip_db = &data.ip_db;
    if query.ip.is_none() && query.country.is_none() {
        return HttpResponse::Ok().json("missing ip and country");
    }
    if query.path.is_none() {
        return HttpResponse::Ok().json("missing path");
    }

    if let Some(country) = &query.country {
        println!("country: {}", country);
        return HttpResponse::Ok().json(dispatch(country, &query, &data));
    } else if let Some(ip) = &query.ip {
        println!("{}", ip);

        let country = ip_db
            .lookup_str(IpAddr::from_str(ip).unwrap())
            .unwrap_or_else(|| "FRA".to_owned());
        println!("country: {}", country);
        return HttpResponse::Ok().json(dispatch(&country, &query, &data));
    } else {
        return HttpResponse::Ok()
            .json("Could not process your request: Found none for ip and country.");
    }
}
