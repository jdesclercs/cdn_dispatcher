use serde::Deserialize;

extern crate serde;
extern crate serde_json;

use std::fs::File;
use std::io::Read;

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct App {
    pub port: String,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct CloudFront {
    pub url: String,
    pub private_key: String,
    pub private_key_pem: Option<Vec<u8>>,
    pub access_key: String,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct StackPath {
    pub url: String,
    pub passphrasefield: String,
    pub passphrase: String,
    pub tokenfield: String,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct OVH {
    pub url: String,
    pub tenant_id: String,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Bunny {
    pub url: String,
    pub access_key: String,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Config {
    pub app: App,
    pub stackpath: Option<StackPath>,
    pub cloudfront: Option<CloudFront>,
    pub ovh: Option<OVH>,
    pub bunny: Option<Bunny>,
}

use super::signed_url::get_cloudfront_key_pem;

pub fn get_config() -> Config {
    let mut file = File::open("./config/config.json").expect("Missing config file");
    let mut buff = String::new();
    file.read_to_string(&mut buff).unwrap();

    let mut config: Config = serde_json::from_str(&buff).unwrap();
    // println!("{:#?}", config);

    if let Some(ref mut cloudfront) = config.cloudfront {
        cloudfront.private_key_pem = Some(get_cloudfront_key_pem(&cloudfront.private_key));
    }
    return config;
}

use super::countries::{CDN_AS_STR, ISO3_TO_ISO2};
use std::collections::HashMap;

pub fn get_cdn_config() -> (
    HashMap<String, fn(&str, &Config) -> String>,
    HashMap<String, fn(&str, &Config) -> String>,
) {
    let mut file = File::open("./config/config-cdn.json").expect("Missing config file");
    let mut buff = String::new();
    file.read_to_string(&mut buff).unwrap();

    let json: HashMap<String, String> = match serde_json::from_str(&buff) {
        serde_json::Result::Ok(val) => val,
        serde_json::Result::Err(err) => {
            panic!("Unable to parse json: {}", err)
        }
    };
    // println!("cdn_config {:#?}", json);

    let mut cdn_from_iso3: HashMap<String, fn(&str, &Config) -> String> = HashMap::new();
    let mut cdn_from_iso2: HashMap<String, fn(&str, &Config) -> String> = HashMap::new();
    for (key, value) in json.into_iter() {
        cdn_from_iso3.insert(key.clone(), *CDN_AS_STR.get(value.as_str()).unwrap());
        cdn_from_iso2.insert(
            ISO3_TO_ISO2.get(key.as_str()).unwrap().to_string(),
            *CDN_AS_STR.get(value.as_str()).unwrap(),
        );
    }

    return (cdn_from_iso2, cdn_from_iso3);
}

use super::dispatcher::Query;
use super::AppState;
use actix_web::{web, HttpResponse};

pub async fn update(query: web::Query<Query>, data: web::Data<AppState>) -> HttpResponse {
    let mut config = data.cdn_config.lock().unwrap();
    *config = get_cdn_config();
    return HttpResponse::Ok().json("Config updated");
}
