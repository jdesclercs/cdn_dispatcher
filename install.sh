sudo apt-get update && sudo apt-get upgrade && sudo apt-get install -y pkg-config
sudo apt-get install openssl
sudo apt-get install libssl-dev
cargo build --release
cp ./target/release/cdn_dispatcher ./
chmod +x cdn_dispatcher
cargo clean
git clone -n https://github.com/sapics/ip-location-db.git --depth 1
cd ./ip-location-db
git checkout origin/master -- ./geo-whois-asn-country/geo-whois-asn-country-ipv4.csv
git checkout origin/master -- ./geo-whois-asn-country/geo-whois-asn-country-ipv4-num.csv
echo "[Unit]
Description=cdn_dispatcher service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=$USER
ExecStart=/bin/bash -c 'cd $PWD && /usr/bin/env ./cdn_dispatcher'

[Install]
WantedBy=multi-user.target" | sudo tee -a /etc/systemd/system/cdn_dispatcher.service